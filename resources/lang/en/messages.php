<?php
return array(
	'system_name' => 'Fiscorp',
    'name'=>'Fiscorp',

    //Menu
    'dashboard'=>'Clientes',
    'byCustomer'=>'Empresas',
    'adminitrators'=>'Admin',
    'enterprises'=>'Empresas',
    'enterprise'=>'Empresa',
    'lists'=>'Plantilla Obligaciones',
    'periods'=>'Catálogo de tiempos',
    'token_absent'=>'Es necesario iniciar sesión',
    'invalid_credentials'=>'Crenciales no válidas',
    'no_authorized_access'=>'Error al intentar acceder a un recurso.',
    'token_invalid'=>'Estás intentando acceder a información no disponible',
    'token_expired'=>'Tu sesión ha expirado',
    'something_wrong'=>'Ocurrió un error durante la petición, vuelve a intentar.',
    'authentication_error' => 'Error de autentificación',
    'logout_m' => 'Sesión cerrada',

    'period'=>[
        'due_day'=>'Día de vencimiento',
        'initial_date'=>'Fecha de inicio',
        'months'=>'Frecuencia',
        'initial_month'=>'Mes en que inicia',
    ],

    'unity'=>[
        'month'=>'Meses',
        'day'=>'Día',
        'last_day'=>'Ùltimo día del mes'
    ],



    'months'=>[
        1=>'Enero',
        2=>'Febrero',
        3=>'Marzo',
        4=>'Abril',
        5=>'Mayo',
        6=>'Junio',
        7=>'Julio',
        8=>'Agosto',
        9=>'Septiembre',
        10=>'Octubre',
        11=>'Noviembre',
        12=>'Diciembre'
    ],


    'monthsIndex'=>[
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
    ],

    'back'=>'Regresar',

    'upToDate'=> [
        'title'=>'Obligaciones',
        'body'=>'Todas tus declaraciones están al día',
        
    ],

    'push'=>[
        'fail'=>'El usuario no ha registrado su token para enviarle notificación.',
        'success'=>'Las obligaciones de la empresa han sido actualizadas, se notificará al usuario.',
        'empty'=>'Todas las obligaciones están actualizadas',
    ],

    'Menu' => [
        [
            'url'=>'dashboard',
            'name'=>'Clientes',
        ],
        [
            'url'=>'admin',
            'name'=>'Admin',
        ]
    ],

    

    'menuEnterprise'=>[
            'name'=>'Obligaciones',
            'submenu'=>
            [
                [
                    'url'=>'lists',
                    'name'=>'Plantilla',
                ],
                [
                    'url'=>'periods',
                    'name'=>'Tiempos',
                ]
            ]
        ],


    'files_of'=>'Archivos de ',

    'profile'=>'Perfil',
    'edit'=>'Editar',
    'exit'=>'Salir',
    'add'=>'Add',

    'error_delete' => 'Error al intentar borrar',
    'error_save' => 'Error al guardar la información.',
    'saved' => 'Información guardada con éxito.',
    'deleted' => 'Eliminado con éxito.',
    'error_validate' => 'Error al guardar, información invalida.',
    'error_not_found' => 'Error al guardar, no se encontro el dato.',
    'error_not_found_edit' => 'Error al guardar, no se encontro el dato a editar.',
    'error_not_found_del' => 'Error, no se encontro el dato a eliminar.',
    'result' => 'Resultado.',
    'unauthorized' => 'No autorizado.',
    'success_login' => 'Inicio de sesión exitoso.',
    'error_login' => 'Error al iniciar sesión.',
    'error_error' => 'Algo fallo, intente de nuevo.',
    'delete_prevented' => 'No se puede eliminar. El dato tiene información relacionada.',
    'user_create_error' => 'Error al crear el cliente. Las contraseñas no coinciden.',
    'save'=>'Guardar',
    'file_error'=>'Error al guardar, debe subir una imagen',
    //Enterprise
    'name'=>'Nombre',
    'address'=>'Dirección',
    'address_1'=>'Calle',
    'address_2'=>'Colonia',
    'user_id'=>'Cliente',
    'image'=>'Subir imagen',
    'company'=>'Elegir imagen de compañía',


    //Tasks
    'tasks'=>'Lista de Obligaciones',
    'active'=>'Terminado',
    'inactive'=>'Obligaciones pendientes',

    'obligations'=>'Seleccionar obligaciones',
    'select_obligation'=>'Seleccione las obligaciones',



    'add_enterprise'=>'Agregar empresa',
    'edit_enterprise'=>'Editar empresa',

    //TaxMailBox
    'tax_mail'=>[
        'error_without_data'=>"Debes mandar parámetros",
        'error_without_user'=>"Ninguna coincidencia de usuarios",
        'error_without_tax'=>'No tienes ninguna coincidencia de Tax Mail',
        'error_save'=>'Error guardando al interntar guardar',
        'title'=>'Buzón tributario',
        'body'=>'Tienes una nueva notificación del buzón tributario.',
    ],
    'error_api'=>'No tiene registros',
    'error_session'=>'Debes iniciar sesión',
    'success_api'=>'',

    'life_periods'=>[
        'display'=>'DURACIÓN',
    ],
    
);