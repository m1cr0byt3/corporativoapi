<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model;
    use Lang;
    use App\Models\TaskDefault;
    use App\User;
    class Enterprise extends Model
    {
        protected $table = 'enterprises';
		protected $primaryKey = 'id';
		public $timestamps = true;
		protected $fillable = array('user_id','name','address_1','address_2','email');


        public function getAddressAttribute($value)
	    {
	        return Lang::get('messages.address_1')."  {$this->address_1} ".Lang::get('messages.address_2')." {$this->address_2}";
        }
        
        public function scopeByUser($query,$id)
	    {
	        return $query->whereUserId($id)->get();
        }
        
        public function user()
	    {
	        return $this->belongsTo('App\User','user_id','user_id');
	    }
		
		public function scopeByEmailTax($query,$emailArray)
	    {
	        return $query->whereIn('email', $emailArray)->get();
	    }

        //Functions

        private function getBaseURL(){
           return env('BASE_URL', 'http://corpofiscal.com');
        }

	    public function getFileAttribute($value)
	    {
            $baseURL = $this->getBaseURL();
	        $url = '/logo/'.$this->id.'/'.$this->file_upload;
	        $url = ($this->file_upload==NULL ? '/assets/images/default/enterprise.png' : $url );
	        return $baseURL.$url;
		}
		
		public function getFileBlankAttribute($value)
	    {
            $baseURL = $this->getBaseURL();
	        $url = '/logo/'.$this->id.'/'.$this->file_upload;
	        $url = ($this->file_upload==NULL ? '/assets/images/default/blank.png' : $url );
	        return $baseURL.$url;
	    }
    }