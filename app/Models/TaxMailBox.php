<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model;
    use Lang;
    use App\Models\Enterprise;
    class TaxMailBox extends Model
    {
        protected $table = 'tail_mail_boxes';
		public $timestamps = true;
        protected $fillable = array('enterprise_id','email_id','has_seen','subject','date','soft_delete');
        
        public function enterprise()
	    {
	        return $this->belongsTo('App\Models\Enterprise','enterprise_id','id');
        }
        
        public function scopeByUser($query,$id)
	    {
	        return $query->whereEnterpriseId($id)->get();
		}
		
		public function scopeByUserApi($query,$id)
	    {
			return $query->whereEnterpriseId($id)
			    ->whereSoftDelete(false)
				->get();
        }
        
        public function scopeByEmailId($query,$email_id)
	    {
	        return $query->whereEmailId($email_id)->first();
        }

        public function scopeHasSeen($query,$value)
	    {
	        return $query->whereHasSeen($value)->get();
        }

        public function scopeGetEmailId($query,$emailArray)
	    {
	        return $query->whereIn('email_id', $emailArray)->get();
        }

    }
?>