<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model;
    use Lang;
	class TaskDefault extends Model
	{
		protected $table = 'task_defaults';
		protected $primaryKey = 'id';
		public $timestamps = true;
		protected $fillable = array('name','life_period_id','due_day','initial_month');

		public function lifeperiod()
	    {
	        return $this->belongsTo('LifePeriod','life_period_id','id');
		} 

		public function getDisplayAttribute($value){
			return "{$this->lifeperiod->display}";
		}

	}
?>