<?php
    namespace App\Models;
    use Illuminate\Database\Eloquent\Model;
	use Lang;
	use App\Models\TaskDefault;
	class Task extends Model
	{
		protected $table = 'tasks';
		protected $primaryKey = 'id';
		public $timestamps = true;
		protected $fillable = array('month','year','enterprise_id','task_default_id','active');

		public function scopeActives($query)
	    {
			return $query->whereActive(1)
			->orderBy('enterprise_id');
	    }

	    public function scopeByEnterprise($query,$id)
	    {
			return $query->whereEnterpriseId($id)
			->orderBy('month','DESC')
			->get();
		}

		public function scopeEnterpriseByActive($query,$id)
	    {
			return $query->whereEnterpriseId($id)
			->whereActive(1)
			->orderBy('active')
			->get();
		}

		public function scopeEnterpriseByInActive($query,$id)
	    {
			return $query->whereEnterpriseId($id)
			->whereActive(0)
			->orderBy('active')
			->get();
		}
		
		public function scopeByEnterpriseInactives($query,$id)
	    {
			return $query->whereEnterpriseId($id)
			->whereActive(0)
			->get();
		}
		
		public function scopeHasHistoryCurrent($query)
	    {
			return $query
			->where("year","=",(date('Y')));
		}

	    public function enterprise()
	    {
	        return $this->belongsTo('App\Models\Enterprise','enterprise_id','id');
		} 
		
		public function taskdefault()
	    {
	        return $this->belongsTo('App\Models\TaskDefault','task_default_id','id');
		} 
		
		
		

		public function getNameAttribute($value){
			return $this->taskdefault->name;
		}

		public function getDisplayAttribute($value){
			return $this->taskdefault->display;
		}

	    public function getisActiveAttribute($value)
	    {	
	        return ($this->active ? Lang::get('messages.active') : Lang::get('messages.inactive'));
	    }

	}
?>