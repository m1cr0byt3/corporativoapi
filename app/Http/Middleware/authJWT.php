<?php
namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Log;
use Lang;

class authJWT
{
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::toUser($request->input('token'));
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return $this->hasError(Lang::get("messages.token_invalid"));
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return $this->hasError(Lang::get("messages.token_expired"));
            }else{
                return $this->hasError(Lang::get("messages.something_wrong"));
            }
        }
        return $next($request);
    }

    private function hasError($message="",$data=[]){
        return response()->json([
            'error' => true,
            'message' => $message,
            'data'=>$data
        ]);
    }
}