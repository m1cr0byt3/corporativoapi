<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use JWTAuth;
use JWTAuthException;
use Lang;
use App\Models\Enterprise;
use App\Models\Task;
use App\Models\TaskDefault;
use App\Models\TaxMailBox;
class ApiController extends Controller
{

    public function __construct()
    {
    }
    
    public function login(Request $request){
        $credentials = $request->only('username', 'password');
        $credentials["password"] = $credentials["password"]."fiscorp";
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'error' => true,
                    'message' => Lang::get("messages.invalid_credentials"),
                    'data'=>[]
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'error' => true,
                'message' => Lang::get("messages.invalid_credentials"),
                'data'=>[]
            ]);
        }
        return response()->json([
            'error' => false,
            'message' => Lang::get("messages.success_login"),
            'data'=>['token'=>$token]
        ]);
    }

    public function isLogedin(Request $request){
       return $this->checkToken();
    }

    public function getEnterprises(Request $request){

        $user =  JWTAuth::authenticate();
        $id = $user->user_id;
        $enterprises = Enterprise::byUser($id);
        $enterprises = $enterprises->map(function ($e) {
        return [
            'id' => $e['id'],
            'name' => $e['name'],
            'address' => $e['address'],
            'file'=> $e['fileBlank'],
            'has_file' => ($e['file_upload']==NULL ? false : true),
            'user_name'=> $e["user"]->email,
            ];
        });
        return $this->success("",$enterprises);
    }

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);        
        return response()->json(['result' => $user]);
    }

    public function logout(Request $request) {
        return $this->success("");
    }

    public function GetTaxMail(Request $request){
        return $this->getTaxMails($request->id);
    }

    private function getTaxMails($id){
        $taxMailBoxes = TaxMailBox::byUserApi($id);
        if($taxMailBoxes->count()>0){
            return $this->success("",$taxMailBoxes);
        }else{
            return $this->success("",[]);
        }
    }

    public function SoftDelete(Request $request){
            $id = $request->id;
            $taxMailBox = TaxMailBox::find($id);
            if($taxMailBox->count()>0){
                $taxMailBox->update(["soft_delete"=>true]);
                $taxMailBox->save();
                    if ($taxMailBox)
                        return $this->getTaxMails($taxMailBox->enterprise_id);
                    else
                    return $this->success("",[]);
            }else{
                return $this->success("",[]);
            }
    }

    private function checkToken(){
        try {
            JWTAuth::parseToken()->authenticate();
      } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        return $this->hasError(Lang::get("messages.token_expired"));
      } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
        return $this->hasError(Lang::get("messages.token_invalid"));  
      } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
        return $this->hasError(Lang::get("messages.something_wrong"));
      }
      return $this->success();    
    }

    public function byEnterprise($id){
        /* Realiza búsqueda de obligaciones fiscales
           por empresa, en el mes actual.			
        */
        $this->checkActive();

        return $this->success("",[
            "tasks"=>$this->getTaskData($id),
            "enterprise"=>$this->getEnterpriseData($id),
            "months"=>$this->orderMonths(),
            "current_month"=>date('m'),
        ]);
    }

    private function getTaskData($id){
        $taskSend = [];
        $tasks = Task::byEnterprise($id);

        foreach($tasks as $e){
            array_push($taskSend,[
                'id'=>$e->id,
                'active'=>$e->active,
                'name'=>$e->taskdefault->name,
                'month'=>$e->month,
            ]);
        }
        return $taskSend;
    }

    private function getEnterpriseData($id){
        $enterprise = Enterprise::find($id);
        return [
            'id' => $enterprise['id'],
            'name' => $enterprise['name'],
            'address' => $enterprise['address'],
            'file'=> $enterprise['file'],
        ];
    }

    private function orderMonths(){
        $months = Lang::get('messages.monthsIndex');
        $newArray = [];
        $currentMonth =  date('m');
        $index = 0;
        foreach ($months as $value) {
            $iterateMonth = $index+1;
            array_push($newArray,[
                'id'=>$iterateMonth,
                'value'=>$value
            ]);
            if($currentMonth==$iterateMonth){
                break;
            }
            $index++;
        }
        $newArray = array_reverse($newArray);
        return $newArray;
    }

    private function checkActive(){
        
    }



    private function success($message="",$data=[]){
        return response()->json([
            'error' => false,
            'message' => $message,
            'data'=>$data
        ]);
    }

    private function hasError($message="",$data=[]){
        return response()->json([
            'error' => true,
            'message' => $message,
            'data'=>$data
        ]);
    }

}