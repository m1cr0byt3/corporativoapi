<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['api']], function () {
    
    Route::post('login', 'ApiController@login');
    Route::get('is/logedin', 'ApiController@isLogedin');
    Route::get('logout', 'ApiController@logout');

    Route::group(['middleware' => 'jwt-auth'], function () {
        //With token
        Route::get('enterprises', 'ApiController@getEnterprises');
        Route::get('enterprise/{id}','ApiController@byEnterprise');
        Route::post('get/taxMail/{id}','ApiController@GetTaxMail');
        Route::post('soft/delete/{id}', 'ApiController@SoftDelete');
    });


});