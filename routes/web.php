<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    return response()->json([
        'error' => false,
        'message' => Lang::get("messages.no_authorized_access"),
        'data'=>[]
    ]);
});

Route::fallback(function(){
    return response()->json([
        'error' => true,
        'message' => Lang::get("messages.no_authorized_access"),
        'data'=>[]
    ]);
});
